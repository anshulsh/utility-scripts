#!/bin/bash

##
# Utility Shell Scripts
#
# LICENSE
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# @license      http://www.gnu.org/licenses/gpl-2.0.html GNU GENERAL PUBLIC LICENSE
##

##
# Development Environment for LAMP in Ubuntu.
# OS: Install a latest 64 bit version of Ubuntu LTS (Currently Ubuntu 12.04)
# You can install any version actually. But these Scripts have been written after test on Ubuntu 12.04
#
# Softwares: Install the following Softwares.
# CAUTION: You should not run this script directly. It is advised to run individual Scripts.
# NOTE: Make sure to enable Canonical Partners in Software Sources.
##

# CAUTION: You should not run this script directly. It is advised to run individual Scripts.
echo "CAUTION: You should not run this script directly. It is advised to run individual Scripts."
exit 1

# Install openssh so that you can connect from other machine.
sudo apt-get install ssh openssh-server openssh-blacklist openssh-blacklist-extra rssh openssh-client screen

# Install light weight utilities for file editing
sudo apt-get install vim vim-scripts vim-doc

# Install utilities for File Compression and Extraction
sudo apt-get install rar unrar gzip

# Install Utilities
sudo apt-get install htop xclip lynx

# Install Curl, Java etc needed for other Software.
# Note: You should probably install latest Java version 7. This installs 1.6
sudo apt-get install curl sun-java6-bin sun-java6-jdk

# Install GIT and other Version Control Tools
sudo apt-get install git gitg git-gui gitk meld

# Install GUI Utilities for Ubuntu
sudo apt-get install gcalcli alacarte gnome-panel gnome-tweak-tool gnome-shell

# Multimedia Utilities. Need Canonical Partners for acroread
sudo apt-get install acroread chromium-browser
